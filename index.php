<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Odyssey
 * @since Odyssey 1.0
 */

get_header(); ?>

	<div id="content" class="site-content" role="main">

    <?php get_template_part( 'blocks/home' ); ?>
    
    <?php get_template_part( 'blocks/releases' ); ?>		

		<?php get_template_part( 'blocks/photos' ); ?>

	<!--	<?php //get_template_part( 'blocks/videos' ); ?>-->


        <?php get_template_part( 'blocks/projects' ); ?>
   <!--  <?php get_template_part( 'blocks/about' ); ?> <?php get_template_part( 'blocks/contact' ); ?> -->
    

</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
