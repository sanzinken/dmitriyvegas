$(document).ready(function(){
  detectOutdatedBrowser();
  phaseSwitching();
  smoothScroll();
  scrollSpy();
  headerMinify();
  
  $('#block_projects-slider').bxSlider({
  	auto: true,

		onSliderLoad: function(){
	    sliderTooltip();
	  }
  });

  $('.home_slider').bxSlider({
  	auto: true,
  	pager: false,
  	controls: false,
  	pause: 7000,
  	speed: 1000
  });

	$(window).resize(function(){
		setTimeout(function() {sliderTooltip();}, 500);
	});

  $('#block_clients-slider').bxSlider({
  	auto: true,

		slideWidth: 170,
		minSlides: 1,
		maxSlides: 5,
		moveSlides: 1,
		slideMargin: 30
	});

	activeNavbar();
	modalConfirm();

	$(window).on('load', function(){
		$(document).scrollTop('0');
		$('.loading_in_progress body').animate({
			opacity: 1
		}, 600, function(){
			$('html').removeClass('loading_in_progress');
			var wow = new WOW({
				mobile: false
			})
			wow.init();
		})
	})
})

function detectOutdatedBrowser(){
  outdatedBrowser({
    bgColor: '#f25648',
    color: '#ffffff',
    lowerThan: 'transform' // 'IE9'
  });
}

function phaseSwitching(){
	
	$('.block_fp-phase').addClass('hidden').first().removeClass('hidden');
	$('.block_fp-nav-item').click(function(e){
		$('.block_fp-nav-item').removeClass('active');
		
		$(this).addClass('active');
		var data = $(this).attr('data');
		$('.block_fp-phase').addClass('hidden');
		$('#'+data).removeClass('hidden');
		$('.block_fp').attr('class',$('.block_fp').attr('class').replace(/\b_ph.*?\b/g, '')).addClass(data);
	});

	setInterval(function(){
		var activeItem = $('.block_fp-nav-item.active').index() + 1,
				nextItem = activeItem + 1,
				listLength = $('.block_fp-nav-item').length;

		if ( nextItem > listLength ) {
			nextItem = 1;
		}

		$('.block_fp-nav-item').removeClass('active');
		$('.block_fp-nav-item:nth-child(' + nextItem + ')').addClass('active');

		$('.block_fp-phase').addClass('hidden');
		$('#_ph'+ nextItem ).removeClass('hidden');

		$('.block_fp').attr('class',$('.block_fp').attr('class').replace(/\b_ph.*?\b/g, '')).addClass('_ph'+ nextItem );

	}, 4000)
}

function activeNavbar(){
	$('._navbar-item').click(function(e){
		$('#main_navbar li').removeClass('active');
		$(this).parent().addClass('active');
	});
} 

function sliderTooltip(){
		i = 1;
	  $('#block_projects-slider .block_projects-slider-item:not(.bx-clone)').each(function(){
			title = $(this).attr('title');
			$('.bx-pager-item:nth-child(' + i + ') a').append('<div class=\"slider_tooltip\">' + title + '</div>');
		 	i ++;
	 });
}

function smoothScroll(){
  $('.smooth_scroll').click(function(e){
    var el = $(this).attr('href'),
        dest = $(el).offset().top - 120;

    $('html, body').animate({scrollTop : dest}, 400);
    e.preventDefault();
  })
}

function scrollSpy(){
  $('body').scrollspy({
  	target: '#main_navbar',
  	offset: 180
  })
}

function modalConfirm(){ 	
	$('#sendConfirm1').click(function(){
		console.log('invalid');
		if (('.wpcf7-form').hasClass('sent')){
			console.log('valid');
		    $('#sendMessage').modal('hide');
		    setTimeout(function(){
		    	$('#modalConfirm').modal('show');
		    }, 300)
		}
	console.log('invalid');
  });
}

function headerMinify(){
	function mini(){
		var scrolled = $(document).scrollTop(),
				winWidth = $(window).innerWidth();

		if ( scrolled > 100 && winWidth > 767 ) {
			$('.header').addClass('minified');
		} else {
			$('.header').removeClass('minified');
		}
	}

	mini();
	$(document).scroll(mini);
}