<?php 
  $page = get_posts(
      array(
          'name'      => 'contact',
          'post_type' => 'page'
      ));

  if ( $page ):  ?>
    <div class="block_contacts text-center" id="contact">
      <div class="block_contacts-content container">
        <h2 class="block_contacts-title wow slideInLeft" data-wow-offset="250">
          <?php echo $page[0]->post_title;?>
        </h2>
        <div class="block_contacts-text wow slideInRight" data-wow-offset="250">
          <p><?php echo $page[0]->post_content;?></p>
        </div>
        <div class="row block_contacts-info wow fadeInUp" data-wow-offset="250">
          <div class="col-xs-11 col-xs-offset-1 text-left">
            <div class="block_contacts-info-item">
              <span>Phone:</span>
              <strong><?php echo get_field('phone',$page[0]->ID)?></strong>
            </div>
            <div class="block_contacts-info-item">
              <span>Fax:</span>
              <strong><?php echo get_field('fax',$page[0]->ID)?></strong>
            </div>
            <div class="block_contacts-info-item">
              <span>​Email:</span>
              <a href="mailto:<?php echo get_field('email',$page[0]->ID)?>"><?php echo get_field('email',$page[0]->ID)?></a>
            </div>
            <div class="block_contacts-info-item">
              <span>Address:</span>
             <?php echo get_field('address',$page[0]->ID)?>
            </div>
            <button class="btn btn-default block_contacts-btn" data-toggle="modal" data-target="#sendMessage">Send a message</button>
          </div>
        </div>
      </div>
    </div>
   <?php endif; ?>