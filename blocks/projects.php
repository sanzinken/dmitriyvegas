<?php 
    $page = get_posts(
        array(
          'name'      => 'projects',
          'post_type' => 'page'
        ));


    if ( $page ):  ?> 
    <div class="block_projects text-center block_fp" id="videos">
<div class="pattern-overlay">
        </div>
<!--      <div class="block_projects-content container wow flipInX" data-wow-offset="350">-->
        <?php
        global $post;
        $args = array(
            'category' => 'video',
            'post_type' => 'post'
        );
        $myposts = get_posts( $args );

        ?>
        <div class="block_projects-slider" style="z-index: 2;">
            <ul id="block_projects-slider">

            <?php

                foreach ($myposts as $postContent) {
                    if( in_category( 'video', $postContent ) ) {?>
                    <li class="block_projects-slider-item">
                        <div class="videoWrapper">
                            <iframe width="1024" height="600"
                                    src="https://www.youtube.com/embed/<?php echo $postContent->post_content; ?>"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    </li>
                <?php }
                    };
            ?>
            </ul>
        </div>

<!--      </div>-->
    </div>
  <?php endif; ?>