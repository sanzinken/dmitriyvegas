<?php 
	$page = get_posts(
    	array(
        	'name'      => 'about',
        	'post_type' => 'page'
    	));

	if ( $page ):  ?>
	    <div class="block_about text-center" id="about">
	      <div class="block_about-content container wow zoomInDown" data-wow-offset="150" data-wow-duration="2s">
	        <h2 class="block_about-title">About</h2>
	        <?php //$images = $dynamic_featured_image->get_post_attachment_ids( $page[0]->ID );?>
	        <div class="about_photo">
	          <div class="about_photo-item">
	            <div class="about_photo-img">
	            	<?php $img = get_field('image',$page[0]->ID);
	            		  $url = $img['url'];
	            	     ?>
	              <img src="<?php echo @$url ?>" alt="Fionnán Friel co-founder">
	            </div>
	            <div class="about_photo-name">
	              <strong> <?php echo get_field('name',$page[0]->ID)?> </strong>
	            </div>
	          </div>
	        </div>
	        <div class="block_about-text">
	          <p>
				<?php echo $page[0]->post_content;?>
	          </p>
	        </div>
	        <div class="block_about-slogan"><?php echo get_field('slogan',$page[0]->ID)?></div>
	        <!--
	        <div class="block_about-logos">
	          <img src="<?php echo get_template_directory_uri(); ?>/img/about_logos_01.png" height="54" width="198" alt="ISPE Gamp">
	          <img src="<?php echo get_template_directory_uri(); ?>/img/about_logos_02.png" height="44" width="134" alt="Compliance">
	        </div>
	        -->
	      </div>
	    </div>
	<?php endif; ?>