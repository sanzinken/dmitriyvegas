<?php 
    $page = get_posts(
      	array(
          'name'      => 'home',
          'post_type' => 'page'
      	));

$post = get_posts(
    array(
        'name'      => 'home-video',
        'post_type' => 'post'
    ));


    if ( $page ):  ?>
		<div id="home">
            <?php
            global $post;

            ?>
		  <div class="block_info text-center">


		      <div class="block_info-mask">
		         <div class="block_info-content -bg_size container">
		          <h2 class="block_info-title">
		          </h2>
		          <div class="block_info-text">
		              <div class="videoWrapper" >
		               	<iframe id="player" width="640" height="360" src="https://www.youtube.com/embed/<?php  echo $post['0']->post_content; ?>?controls=0&enablejsapi=1&modestbranding=1&showinfo=1&nologo=1&rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>
		              </div>
		          </div>
		        </div>
		      </div>
		    </div>

		</div>

	<?php endif;?>

<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/player.js"></script>
<script>
    var player;
    function onYouTubePlayerAPIReady() {player = new YT.Player('player');console.log(player);}

</script>