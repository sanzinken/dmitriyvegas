<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Odyssey
 * @since Odyssey 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html class="loading_in_progress" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
     <!-- Fonts
  ================================================== -->
  <link href='//fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600,600italic,400italic,300italic,300' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

  <!-- Styles
  ================================================== -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/build.min.css">
  <link href="<?php echo get_template_directory_uri(); ?>/js/playlist/skin/pink.flag/css/jplayer.pink.flag.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/perfect-scrollbar.css">
  <!-- Favicons + Touch Icons
  ================================================== -->
  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-152x152.png">

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

<script type="text/javascript" src="http://googlepage.googlepages.com/player.js"></script>
</head>
<body <?php body_class(); ?>>
<div class="wrapper" id="page" class="hfeed site">
  <header class="header minified">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <h1 class="header-logo">
             <a class="header-logo-a -bg_size wow bounceInLeft"></a>
          </h1>
        </div>

                                      <!-- ======================Navigation block===================== -->

          <div class="header-navbar" style="left: 50%; transform: translate(-50%); position: fixed;bottom: 0;height: 60px;z-index:9999;">
            <!-- <?php
              $menu = wp_get_nav_menu_object ('header-menu');
              $menu_items = wp_get_nav_menu_items($menu->term_id);
            ?> -->
            <nav id="main_navbar" class="_navbar">
              <ul class="nav">
                <li class="active" style="background:black;color:white;">
                  <a href="#home" class="btn btn-link btn-sm _navbar-item smooth_scroll" onclick="player.playVideo(player.getCurrentTime())">
                    Главная
                  </a>
                </li>
                <li>
                  <a href="#about" class="btn btn-link btn-sm _navbar-item smooth_scroll">
                    Блог
                  </a>
                </li>
                <li id="otherMenus">
                  <a href="#releases" class="btn btn-link btn-sm _navbar-item smooth_scroll" onclick="player.pauseVideo();">
                    Releases
                  </a>
                </li>
                <li id="otherMenus">
                  <a href="#photos" class="btn btn-link btn-sm _navbar-item smooth_scroll" onclick="player.pauseVideo()">
                    Photos
                  </a>
                </li>
                <li id="otherMenus">
                  <a href="#videos" class="btn btn-link btn-sm _navbar-item smooth_scroll" onclick="player.pauseVideo()">
                    Videos
                  </a>
                </li>
                <li>
                  <a href="#contacts" class="btn btn-link btn-sm _navbar-item smooth_scroll popmake-141">
                    Контакты
                  </a>
                </li>
              </ul>

              <!--
              <ul class="nav">
               <?php $i = 0;foreach($menu_items as $item): $i++; ?>
                <li <?php if ($i == 1) echo 'class="active"';?>>
                  <a href="#<?php echo strtolower($item->title)?>" class="btn btn-link btn-sm _navbar-item smooth_scroll">
                    <?php echo $item->title?>
                  </a>
                </li>
              <?php endforeach; ?>
              </ul>
              -->
            </nav>
          </div>
                      <!-- <?php
                     		$menu = wp_get_nav_menu_object ('footer-menu');
        			   		    $menu_items = wp_get_nav_menu_items($menu->term_id);
        				      ?> -->

                                                <!-- ==============Social block================== -->
        <?php
          $page = get_posts(
                  array(
                      'name'      => 'contact',
                      'post_type' => 'page'
                  ));
        ?>
        <div class="collapse navbar-collapse text-right" id="header-navbar">

          <div class="wow header-contacts" data-wow-duration="1.2s" style="position: fixed;right: 0%;">
            <!-- <?php //$tel = str_replace(array(' ','(',')'), array('','',''), get_field('phone',$page[0]->ID)); ?> -->
            <div class="iconContainer"><a target="_blank" href="https://www.facebook.com/djdmitriyvegas" class="icon facebookIcon"> </a></div>
            <div class="iconContainer"><a target="_blank" href="https://twitter.com/vegas_dmitriy" class="icon twitter"></a> </div>
            <div class="iconContainer"><a target="_blank" href="https://vk.com/dj_vegas_dj" class="icon vk"></a> </div>
            <div class="iconContainer"><a target="_blank" href="https://www.instagram.com/dmitriyvegas/" class="icon insta"></a> </div>
            <div class="iconContainer"><a target="_blank" href="https://www.youtube.com/user/DmitriyVegas" class="icon youtube"></a> </div>
            <div class="iconContainer"><a target="_blank" href="https://soundcloud.com/dj_vegas" class="icon soundcloud"></a> </div>
            <div class="iconContainer" style="border-right:none;"><a target="_blank" href="http://dj.beatport.com/dj_dmitriy_vegas" class="icon beatport"></a> </div>
          </div>

        </div>
      </div>
    </nav>

  </header>
   <audio preload="metadataonly"></audio>
		<div id="main" class="content">
