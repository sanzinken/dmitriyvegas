<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->

		<!-- Footer
		================================================== -->
		<footer class="footer" style="overflow-x: hidden;">
<script src="<?php echo get_template_directory_uri(); ?>/js/player.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
        function OnMouseIn (elem) {
          elem.style.bottom='35px';

        }
        function OnMouseOut (elem) {
            elem.style.bottom='5px';
        }
	function playVideo(){
		var videoURL = $('#player').prop('src');
		videoURL += "&autoplay=1";
		$('#player').prop('src',videoURL);
}
	function stopVideo(){
		var videoURL = $('#playerID').prop('src');
		videoURL = videoURL.replace("&autoplay=1", "");
		$('#player').prop('src','');
		$('#player').prop('src',videoURL);

}
function playTrack(track){
$('.foot-player').empty();
$('.foot-player').append("<iframe width='100%' height='100%' id='SoundCloudPlayer' frameborder='no' src='https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/"+track+"&amp;auto_play=true&amp;hide_related=false&amp;show_comments=false&amp;show_user=true&amp;show_reposts=false&amp;visual=true;sharing=false'></iframe>");
console.log($('#SoundCloudPlayer'));
//var newPlayer=$('#SoundCloudPlayer').innerHTML.replace('14px 10px;z-index:100}', '14px 10px;z-index:0}');
//$('.foot-basket').append(newPlayer);
}
function playVideo(video){

videoUrl='https://www.youtube.com/embed/'+video+'?controls=0&enablejsapi=1&modestbranding=1&showinfo=1&nologo=1&rel=0&autoplay=1';
$('#player')['0']['src']=videoUrl;

}
$('[data-spy="scroll"]').each(function () {
    $(this).scrollspy('refresh');
});

  </script>
<div class="foot-player" style="width:100%; opacity: .5 ;position: fixed;bottom:5px;height: 70px; background:black;transition: all .2s ease-in-out; overflow: hidden;"  onmouseover="OnMouseIn (this)" onmouseout="OnMouseOut (this)" >

 </div>

		</footer>

		<!-- Modals
		================================================== -->


			<div class="modal modal_send_message" id="sendMessage">

		  <div class="modal-dialog modal-lg">
		    <div class="modal-content text-center">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
		        <h4 class="modal-title">Send a message</h4>
		      </div>
		      <div class="modal-body">
		      	<p>We’re an energetic, flexible, and open-minded team ready to work hard for our clients. If you’re interested in working with us, please send us a message!</p>
		        <?php echo do_shortcode('[contact-form-7 id="104" title="Send Message Form"]'); ?>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div class="modal modal_send_confirm" id="modalConfirm">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content text-center">
		      <div class="modal-header">
		        <h4 class="modal-title">
		        	<img src="<?php echo get_template_directory_uri(); ?>/img/layout/modal_send.svg" alt="send">
		        </h4>
			     </div>
		      <div class="modal-body">
		        <p>Your message has been sent successfully!</p>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button><br>
		      </div>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		**/ ?>
	</div><!-- #page -->

	<div id="outdated">
  		<a id="btnUpdateBrowser"
  		<p class="last"><a href="#" id="btnCloseUpdateBrowser" title="Close">&times;</a></p>
	</div>
	<!-- Javascript
	================================================== -->

	<!-- Connecting jQuery library from google -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- If google unavailable - connect local copy of jQuery -->
	<script>window.jQuery || document.write('<script src="js/assets/jquery-1.11.1.min.js"><\/script>')</script>
	<!-- Plugins goes here -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/build.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/flip.js"></script>
	<!--<script src="<?php echo get_template_directory_uri(); ?>/js/assets/audiojs/audio.min.js"></script>-->
    <!--<script src="<?php echo get_template_directory_uri(); ?>/js/playlist.js"></script>-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/perfect-scrollbar.jquery.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/playlist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/playlist/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript">
	    $(document).ready(function(){
	    	$('.scrollable').perfectScrollbar();
var x = location.hash;
clickOnHash = function(x){
console.log('click');
	$('a[href="'+x+'"]').click();
	console.log(x);
};
console.log(x);
if(x) {
	setTimeout(clickOnHash , 3000,x);

}
	    });
    </script>
	<?php wp_footer(); ?>
</body>
</html>
